package com.shankar.securitydb.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shankar.securitydb.model.Users;

public interface UsersRepository extends JpaRepository<Users, Integer> {
    Optional<Users> findByName(String username);
}
